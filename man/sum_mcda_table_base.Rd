% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/fct_summary.R
\name{sum_mcda_table_base}
\alias{sum_mcda_table_base}
\title{Create summary for datatable for total values}
\usage{
sum_mcda_table_base(data_partial, data_end)
}
\arguments{
\item{data_partial}{df_mcda_partial()}

\item{data_end}{df_mcda_end()}
}
\value{
tibble with summary
}
\description{
Create summary for datatable for total values
}
\keyword{internal}
