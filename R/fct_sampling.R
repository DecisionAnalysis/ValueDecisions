# ValueDecisions
# Copyright 2020 Eawag and Fridolin Haag, Contact: judit.lienert@eawag.ch
#   
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

#' Create samples for each uncertainty distribution, looping throught the rows
#'
#' @param data prediction data frame, as specified
#' @param nsamp number of samples to generate
#'
#' @return tibble with a list column of samples (ests) and a list column of sample indices (run)
#' @importFrom magrittr %>%
#' @importFrom rlang .data
#' @keywords internal
sample_pred_rowwise <- function(data, nsamp) {
  data %>%
    dplyr::rowwise() %>%
    dplyr::mutate(
      ests = list(sample_distr(
        distr = .data$unc_distribution,  
        par1 = .data$unc_par1, 
        par2 = .data$unc_par2, 
        par3 = .data$unc_par3, 
        discr_levels = .data$attr_levels,
        p_levels = .data$prob_levels,
        rmin = .data$range_min, 
        rmax = .data$range_max, 
        pred = .data$prediction, 
        nsamp = nsamp)),
      run = list(1:nsamp)
    ) %>%
    dplyr::ungroup() %>%
    dplyr::select(.data$option, .data$attribute, .data$unit, .data$ests, .data$run)
}

#' Sample from uncertainty distribution
#'
#' @param distr distribution
#' @param par1 parameters of distribution, see user guide
#' @param par2 parameters of distribution, see user guide
#' @param par3 parameters of distribution, see user guide
#' @param discr_levels parameters of distribution, see user guide
#' @param p_levels parameters of distribution, see user guide
#' @param rmin range min
#' @param rmax range max
#' @param nsamp  number of samples to create
#' @param pred point estiamte prediction
#' @param ... usually nothing
#'
#' @return vector with samples
#' @importFrom stats runif rbeta
#' @keywords internal
sample_distr <- function(distr, par1, par2, par3, discr_levels, p_levels, rmin, rmax, nsamp, pred, ...) {
  if (is.na(distr) | distr == "none") { # no unc
    return(rep(pred, nsamp))
  } else if (distr == "normal") {
    stopifnot(!is.na(par1) & !is.na(par2))
    if (par2 == 0) {return(rep(pred, nsamp))} # SD = 0
    EnvStats::rnormTrunc(nsamp, mean = par1, sd = par2, min = rmin, max = rmax)
  } else if (distr == "uniform") {
    stopifnot(!is.na(par1) & !is.na(par2))
    if (par1 < par2) {runif(nsamp, par1, par2)}
    else if (par1 > par2) {runif(nsamp, par2, par1)}
    else if (par1 == par2) {rep(par1, nsamp)}
  } else if (distr == "beta") {
    stopifnot(!is.na(par1) & !is.na(par2))
    stopifnot(par1 > 0, par2 > 0)
    rbeta(n = nsamp, shape1 = par1, shape2 = par2)
  } else if (distr == "lognormal") {
    stopifnot(!is.na(par1), !is.na(par2))
    EnvStats::rlnormTrunc(n = nsamp, meanlog = par1, sdlog = par2, min = rmin, max = rmax)
  } else if (distr == "triangular") {
    stopifnot(!is.na(par1) & !is.na(par2) & !is.na(par3))
    EnvStats::rtri(n = nsamp, min = par1, max = par2, mode = par3)
  } else if (distr == "gevd") { # Weibull & Gumbel
    stopifnot(!is.na(par1) & !is.na(par2) & !is.na(par3))
    stopifnot(par2 > 0)
    truncdist::rtrunc(n = nsamp, spec = "gev", a = rmin, b = rmax, loc = par1, scale = par2, shape = par3)
  }  else if (distr == "discrete") {
    # Convert the strings to numerical vectors:
    levels <- as.numeric(
      gsub(",", ".", # Replace commas from German decimal numbers
           unlist(strsplit(discr_levels, split = "\\/"))
      )
    )
    probabilities <- as.numeric(
      gsub(",", ".", # Replace commas from German decimal numbers
           unlist(strsplit(p_levels, split = "\\/"))
      )
    )
    # Sample:
    sample(x = levels, size = nsamp, replace = TRUE, prob = probabilities) 
  }  else {
    stop("Error: Please use valid probability distributions in the prediction file.")
  }
}
