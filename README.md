# ValueDecisions

The [ValueDecisions app](https://www.eawag.ch/en/department/ess/main-focus/decision-analysis-da/tools/valuedecisions-app/) is a tool to support you in finding solutions for complex decision problems with multiple criteria: where making a good choice is not easily possible simply by gut feeling and common sense. Use ValueDecisions to explore decision problems from any field: environmental management, public policy, urban planning, technical engineering, medicine, business administration, or personal choices. ValueDecisions is especially useful for decisions with high uncertainty that affect many stakeholders with conflicting interests.

## Installation

To run ValueDecisions, you need to have [R](https://cran.r-project.org/index.html) installed on your system.

You can install the latest version of ValueDecisions from [GitLab](https://gitlab.switch.ch/DecisionAnalysis/ValueDecisions) from an R console with:

``` r
install.packages("remotes")
remotes::install_git("https://gitlab.switch.ch/DecisionAnalysis/ValueDecisions")
```

Note that ValueDecisions currently has many dependencies on other packages. These will be installed from [CRAN](https://CRAN.R-project.org) during package installation. This can take a while.

## Launching the app

You can launch the app with:

``` r
ValueDecisions::run_app()
```
## Online version
The online version of the app can be accessed at https://eawag.shinyapps.io/ValueDecisions/. The productive use of the app is protected by an access code. A code can be requested from [Judit Lienert](mailto:judit.lienert@eawag.ch).

## Citing the app

If you use the ValueDecisions app, please cite it as follows: 

Haag, F., Aubert, A.H., Lienert, J., 2022: ValueDecisions, a web app to support decisions with conflicting objectives, multiple stakeholders, and uncertainty. *Environmental Modelling & Software* 105361. https://doi.org/10.1016/j.envsoft.2022.105361

## Background information

The ValueDecisions app is an open-source software, developed at Eawag, to calculate the results of Multi-Criteria Decision Analysis (MCDA). It is based on Multi-Attribute Value Theory (MAVT).
The ValueDecisions app allows you to specify data for different decision options, different objectives (and attributes), and different stakeholders. All you need to upload are two Excel sheets. One contains the predictions of consequences of decision options (and their uncertainty estimates, if available). The other contains stakeholder preference data, if available, namely the weights, single-attribute value functions, and the MCDA aggregation model.

The ValueDecisions app is designed to deal with the inherent uncertainty of complex (environmental) decisions. The uncertainty of the predictions (i.e., how each option performs) is modelled with Monte Carlo simulation, based on the uncertainty distributions entered in the predictions Excel sheet. The uncertainty of the stakeholder’s preferences can be explored with various sensitivity analyses (of the weights, shape of single-attribute value functions, and aggregation model). The ValueDecisions app visualizes the results with graphs and tables. The results can be downloaded directly, or as Word report.

You can find [more information on the website](https://www.eawag.ch/en/department/ess/main-focus/decision-analysis-da/tools/valuedecisions-app/) as well as the app itself.

## License

ValueDecisions is provided under a AGPL-3 license (see license file for details).

### Authors
Concept & Design: Fridolin Haag and Judit Lienert

Programming: Fridolin Haag, Kevin Schönholzer, Sara Schmid, ETH Scientific IT Services

User guidance: Judit Lienert and Daniel Hofmann

Copyright:
The authors and Eawag: Swiss Federal Institute of Aquatic Science and Technology, Überlandstrasse 133, 8600 Dübendorf, Switzerland. 

## Specific topics

### Bugs and issues

If you encounter bugs or any other problems please report them at [GitLab](https://gitlab.switch.ch/DecisionAnalysis/ValueDecisions) or via email to [Fridolin Haag](mailto:fridolin.haag@leibniz-zmt.de).

### Package structure

This project follows a R package structure. It follows the [golem](https://github.com/ThinkR-open/golem/) framework for bundling an app in the form of an R package.

#### Inputs and outputs
There are no inputs necessary. Inputs are supplied by the users.
There are not outputs saved automatically. Outputs have to be saved by the users.

#### R Scripts
All functionality is contained in Rscripts in the folder `R`.

An additional file `app.R` is used for deployment if the app is not run locally.

#### www
In the folder `inst/app/www`, additional files for the app are provided. 

* `styles.css`: changes to the default CSS styles are specified to have the app looking the way we want.
* `report.Rmd`: Rmarkdown file for the reporting. The file is rendered when the report is called via the app.
* `template.docx`: Template for the report.
* `read_html.lua`: Needed for using html tags in the report.
* `texts_db.xlsx`: Database of longer texts. Allows html tags within texts.
* `ValueDecisions_preferences_templ_holiday`: Templates of input files that can be downloaded by the user.
* `ValueDecisions_predictions_templ_holiday`: Templates of input files that can be downloaded by the user.

Additional files are served from the Eawag server and will only appear if connected to the internet.

### Deployment

The app can be, and currently is, deployed on [shinyapps.io](https://www.shinyapps.io/) under the address https://eawag.shinyapps.io/ValueDecisions/. 

There is an extensive user guide for this platform: https://docs.rstudio.com/shinyapps.io/. The app can be published from within RStudio. A "how to" for this [can be found here](https://docs.rstudio.com/shinyapps.io/getting-started.html#deploying-applications).
